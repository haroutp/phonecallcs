﻿using System;

namespace PhoneCall
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(phoneCall(3, 1, 2, 20));
        }



        static int phoneCall(int min1, int min2_10, int min11, int s) {
            int minutes = 0;
            if(min1 > s){
                return 0;
            }
            
            s -= min1;
            if(s / min2_10 <= 9){
                minutes = s / min2_10 + 1;
                return minutes;
            }
            
            s -= min2_10 * 9;
            minutes = s / min11 + 10;
            return minutes;
        }



        // static int phoneCall(int min1, int min2_10, int min11, int s) {
        //     if(min1 > s){
        //         return 0;
        //     }
            
        //     s -= min1;
        //     if(s / min2_10 <= 9)
        //         return ((s/min2_10) + 1);
            
        //     s -= min2_10 * 9;
            
        //     return ((s / min11) + 10);
        // }

    }
}
